////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#ifndef OBJECTIVES_H
#define OBJECTIVES_H

#include "stafx.h"

#define MAX_OBJECTIVES 10

#define OBJECT_RADIUS 2.6

class PlayerUI;
class Object;
class FirstTerrain;

class Objectives{

private:
	Object *m_aObs[MAX_OBJECTIVES];

	void placeObjectives();
	void playGoalSnd();

	bool		m_bComplete;
	int			m_iCount;
	int			m_iCollect;
	float		m_fMinDist;
	float		m_fMaxDist;
	int			m_iGoalRad;
	int			m_iTestRad;
	//going to edit the height map based on the placement of objectives
	FirstTerrain	*m_pCrntMap;
	SoundManager	*m_pSndMan;

	ALuint		snd1;
	ALuint		snd2;

public:

	Objectives();
	~Objectives();
	Object*			getObject( int index )			{	if(index < m_iCount)return m_aObs[index];	};
	void Init( FirstTerrain* pMap );
	void Update();
	void Render( GLShaderManager &shaderManager, GLGeometryTransform &transformPipeline, GLMatrixStack &modelViewMatrix, M3DMatrix44f mCamera );


	bool getComplete()			{	return m_bComplete;	};
	bool checkPlayer( PlayerUI* pPlayer );
};

#endif


