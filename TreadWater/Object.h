////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#ifndef OBJECT_H
#define OBJECT_H

#include "stafx.h"

#define		GOAL_TRIG	167

class Object{

public:

	Object(char* modelFname, char* texFname);
	~Object();

	bool init( int startX, int startY ,int startZ, float iSize );
	bool update();
	void render( GLShaderManager &shaderManager, GLGeometryTransform &transformPipeline, GLMatrixStack &modelViewMatrix, M3DMatrix44f mCamera );
	void render( GLShaderManager &shaderManager, GLGeometryTransform &transformPipeline, GLMatrixStack &modelViewMatrix );

	void setRadius( float newRad )		{	m_fRadius = newRad;	};

	bool checkPlayer( PlayerUI *pPlayer, int testRad );

	float getRad()				{	return m_fRadius;	};
	float getPosX()				{	return (m_frPos.GetOriginX());	};
	float getPosY()				{	return (m_frPos.GetOriginY());	};
	float getPosZ()				{	return (m_frPos.GetOriginZ());	};

	void getBoxP1( M3DVector3f& v )				{	m3dCopyVector3(v, boxP1);	};
	void getBoxP2( M3DVector3f& v )				{	m3dCopyVector3(v, boxP2);	};

	void incrGoal()				{	if(!m_bGoalTrig) ++m_iGoalCount;	};

private:

	float				m_fRadius;//just squares or circles!
	md2model*			m_cModelLoader;

	M3DVector4f			m_vLightEyePos;
	M3DVector4f			m_vLightPos;

	M3DVector3f			boxP1;
	M3DVector3f			boxP2;

	GLuint				m_iTexture;
	int					m_icrntAnim;
	Spawn*				m_cSpawn;
	GLFrame				m_frPos;
	int					m_iGoalCount;
	bool				m_bGoalTrig;
};

#endif