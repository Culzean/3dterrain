////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#ifndef	TERRAIN_BATCH_H
#define TERRAIN_BATCH_H


#define WIN32

#ifdef WIN32
#include <windows.h>		// Must have for Windows platform builds
#ifndef GLEW_STATIC
#define GLEW_STATIC
#endif

#include <gl\glew.h>			// OpenGL Extension "autoloader"
#include <gl\gl.h>			// Microsoft OpenGL headers (version 1.1 by themselves)
#endif

#include <math3d.h>
#include <GLBatchBase.h>
#include <GLShaderManager.h>

#define VERTEX_DATA     0
#define NORMAL_DATA     1
#define TEXTURE_DATA    2
#define INDEX_DATA      3

class TerrainBatch : public GLBatchBase{
	 public:
        TerrainBatch(void);
        virtual ~TerrainBatch(void);
        
        // Use these three functions to add triangles
        void BeginMesh(GLuint nMaxVerts);
        void AddTriangle(M3DVector3f verts[3], M3DVector3f vNorms[3], M3DVector2f vTexCoords[3]);
        void End(void);

        // Useful for statistics
        inline GLuint GetIndexCount(void) { return nNumIndexes; }
        inline GLuint GetVertexCount(void) { return nNumVerts; }

		void FindSurfaceNormal( M3DVector3f normal, M3DVector3f ver1, M3DVector3f ver2, M3DVector3f ver3 );
        
        // Draw - make sure you call glEnableClientState for these arrays
        virtual void Draw(void);
        
protected:

        GLushort  *pIndexes;        // Array of indexes
        M3DVector3f *pVerts;        // Array of vertices
        M3DVector3f *pNorms;        // Array of normals
        M3DVector2f *pTexCoords;    // Array of texture coordinates
        
        GLuint nMaxIndexes;         // Maximum workspace
        GLuint nNumIndexes;         // Number of indexes currently used
        GLuint nNumVerts;           // Number of vertices actually used
        
        GLuint bufferObjects[4];
		GLuint vertexArrayBufferObject;
};


#endif