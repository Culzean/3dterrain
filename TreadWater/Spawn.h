////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#ifndef SPAWN_H
#define SPAWN_H

#include "stafx.h"
#include <GLFrame.h>
#include <GLMatrixStack.h>
#include <GLShaderManager.h>
#include <GLMatrixStack.h>

class Spawn{
public:
	Spawn();
	Spawn(	char* modelFname, char* texFname, GLuint tex );
	~Spawn();
	void setModel(md2_model_t* newModel)		{ myModel = newModel; };
	

	md2_model_t* GetModel()						{ return myModel; };

	void	setTexture(GLuint newTex )			{	texture = newTex;	};
	GLuint	getTexture()						{	return texture;	};

private:
	md2_model_t*	myModel;

	GLuint				texture;	
	GLuint				currentAnim;

	bool loadModel(char* modelFname, char* texFname);
};

#endif SPAWN_H