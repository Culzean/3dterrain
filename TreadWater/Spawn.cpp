////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#include "spawn.h"

Spawn::Spawn()
{
	myModel = NULL;
}

Spawn::Spawn(char* modelFname, char* texFname, GLuint myTex)
{
	texture = myTex;
	loadModel( modelFname, texFname );
}

Spawn::~Spawn()
{
	//delete myFrame;
	myModel = NULL;//deleted through the map data structure
}

bool Spawn::loadModel(char* modelFname, char* texFname)
{
	md2model* loader = md2model::getInstance();

	myModel = loader->GetMD2Model( modelFname );
	if(myModel != NULL)
		return true;
	else
		return false;
}

