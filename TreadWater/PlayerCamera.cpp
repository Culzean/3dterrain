////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#include "PlayerCamera.h"
#include "PlayerUI.h"
#include "Tools.h"
#include <math.h>

PlayerCamera::PlayerCamera(PlayerUI* playerRef)
{
	m_pPlayer = playerRef;
	init();
}

PlayerCamera::~PlayerCamera()
{
	//delete m_MatrixMV;
}

bool PlayerCamera::init()
{
	m_frCamera.SetOrigin(0.0f,0.0f,0.0f);
	m_frCamera.SetUpVector(0.0f,1.0f,0.0f);
	mouseX = 0; mouseY = 0;
	viewAngle = 0.0f;
	rotAngle = 0.0f;

	m_vForward[2] = 0.0f;

	camAngle = 0.0f;
	//cameraX = 0.0f;		cameraZ = 0.0f, cameraY = 3.3;

	return true;
}

void PlayerCamera::setForward()
{
	m_frCamera.GetForwardVector(m_vForward);

}

void PlayerCamera::update(GLFrame playerOrg, float newHeight)
{
	M3DVector3f	newOrigin;
	M3DVector3f	playerOrigin;
	M3DVector3f	lookV, sideVec;
	M3DVector3f upVec = {0.0f, 1.0f, 0.0f};

	playerOrg.GetOrigin(playerOrigin);

	newOrigin[0] = playerOrigin[0] + 7 * -cos(viewAngle);
	newOrigin[1] = playerOrigin[1] + (12 * -sin(camAngle));
	newOrigin[2] = playerOrigin[2] + 12 * -sin(viewAngle);

	lookV[0] = playerOrigin[0] - newOrigin[0];
	lookV[1] = playerOrigin[1] - newOrigin[1];
	lookV[2] = playerOrigin[2] - newOrigin[2];
	m3dNormalizeVector3(lookV);

	m_vForward[0] = lookV[0];
	m_vForward[1] = lookV[1];
	m_vForward[2] = lookV[2];

	m_frCamera.SetOrigin(newOrigin);
	m_frCamera.GetOrigin(m_vOrigin);
	m_frDir.SetOrigin(newOrigin);

	m_frDir.SetForwardVector(lookV);
	m3dCrossProduct3(sideVec, lookV, upVec);

	m3dNormalizeVector3(sideVec);

	//recalculate up vec
	m3dCrossProduct3(upVec, sideVec, lookV);

	m_frDir.SetUpVector(upVec);
}

GLFrame PlayerCamera::getHighFrame( int iMap )
{
	M3DVector3f highPoint;

	highPoint[0] = - iMap;
	highPoint[1] = 100;
	highPoint[2] = - iMap* 0.5;

	m_frHigh.SetOrigin(highPoint);

	return m_frHigh;
}

void PlayerCamera::keyOperations( unsigned char key, int x, int y )
{

}

void PlayerCamera::mouseMotion( int x, int y )
{
	GLfloat xCom = (mouseX - x) * 0.005f;
	GLfloat yCom = (mouseY - y) * 0.005f;

	GLFrame		playerFrame;
	if(abs(xCom) > abs(yCom)){
		rotAngle = xCom;
		viewAngle += xCom;
	}else
		camAngle += yCom;

	//cout << "myAngle: " << viewAngle << endl;

	if(viewAngle > M_PI * 2)
		viewAngle = 0 + xCom;
	if(viewAngle < 0)
		viewAngle = M_PI * 2 + xCom;


	mouseX = x; mouseY = y;
}


void PlayerCamera::mouseFunc(int button, int state,int x, int y)
{
	if(GLUT_LEFT_BUTTON == button && state == GLUT_DOWN)
	{
		mouseX = x;		mouseY = y;
	}
}