#include "TextureManager.h"

using namespace std;

int TextureManager::lastID = 0;

TextureManager* TextureManager::pTexManager = 0;


TextureManager::TextureManager()
{
	//first texture to load is null texture.
	//if this cannot be found we have trouble
	loadTexture("res/NullTexture.tga");
}

GLuint TextureManager::getTexID(const char* fname)
{
	index = Texmap.find(fname);

	if(index == Texmap.end())
		{
			if(loadTexture(fname) == -1)//load failed. attempt to load NULL texture
			{
				cout << "This texture cannot be found" << endl;
				index = Texmap.find("NullTexture.tga");
					if(index == Texmap.end())
					{
						cout << "Backup texture cannot be found!! Failed to load texture anything";
						return -1;//total failure. soomething has been put together wrong
					}else
						return Texmap["NullTexture.tga"];
			}
		}

		return Texmap[fname];
}


GLuint TextureManager::loadTexture(const char* fname)
{
	//need check for duplicate load requests
	//store file name in cube class?
	//on remove object check that this texture is still needed?
	//on add object check if its texture is loaded.
	//if not
	//load object->getFname
	//cannot find fname then use nullTexture
	GLuint* texID = new GLuint;
	// generate texture ID
	glGenTextures(1, texID);
	// texture dimensions and data buffer
	GLint width, height, components;
	GLenum format;
	GLbyte *pBytes;

	// load file - using GLTools library
	pBytes = gltReadTGABits(fname, &width, &height, &components, &format);
	if (pBytes == NULL)
	{
		cerr << "Something went wrong loading texture " << fname << endl;
		return -1;
	}
	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, *texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D,0,components,width, height, 0,
		format, GL_UNSIGNED_BYTE, pBytes);
	glGenerateMipmap(GL_TEXTURE_2D);
	// texture loaded, free the temporary buffer
	free(pBytes);

	//fname is the key. Add this texture to the map
	Texmap.insert(pair<string, GLuint>(fname,*texID));

	delete texID;
	cout << "New texture loaded : " << fname << endl;
	return getTexID(fname);	// return value of texure ID
}

void TextureManager::cleanUp()
{
	index = Texmap.begin();
	//for(; index != Texmap.end(); index++)
	{
		glDeleteTextures(Texmap.size(), ( &(*index).second ));
	}
}

TextureManager::~TextureManager(){
		

	}