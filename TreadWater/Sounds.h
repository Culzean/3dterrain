#ifndef SOUNDS_H
	#define SOUNDS_H

	#include <bass.h>
	#include <iostream>
	using namespace std;

	#define SAMPLES_NUMBER 6
	#define BACKGROUND_MUSIC 0
	#define HORN_SOUND 1
	#define BATTLE_SOUND 2
	#define ATTACK1_SOUND 3	
	#define ATTACK2_SOUND 4
	#define ATTACK3_SOUND 5	

	class Sounds
	{
		public:
				//Create file to load stream
				HSTREAM loadStream(char *filename);

				//Constructor
				Sounds();
				~Sounds();

				//Init BASS
				void initBass();				

				//Play the sound which is given in parameter
				void playSound(int soundNumber);
		private:
				HSTREAM* streams;
				HCHANNEL ch;
				HSTREAM stream;
	};
#endif
