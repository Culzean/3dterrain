////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#ifndef PLAYER_UI
#define PLAYER_UI

#include "stafx.h"

class Spawn; class Object;

using namespace std;

#define MAX_FALL_VEL 0.98
#define MAX_CLIMB_VEL 0.15

#define FRICTION_SUB 0.09

#define		NO_WALK_SONDS	3

class PlayerCamera;// no body! just a note these classes do exist
class FirstTerrain;

class PlayerUI{
public:
	PlayerUI();
	PlayerUI(	char* modelFname, char* texFname, PlayerCamera* cameraRef, FirstTerrain* currentLvlRef	);
	~PlayerUI();

	/////////////////////
	//controls
	void mouseMotion();
	void keyBoard( int key, bool val );
	void KeyUp(unsigned char key, int x, int y);
	void KeyDown(unsigned char key, int x, int y);
	void KeyOperations(void);

	bool init( int mapCenX, int mapCenZ );


	void update();
	void render( GLShaderManager &shaderManager, GLGeometryTransform &transformPipeline, GLMatrixStack &modelViewMatrix, M3DMatrix44f mCamera );
	bool checkCollision( Object* pO );

	GLFrame	GetFrame()						{ return m_Frame; };
	void setFrame(GLFrame newFrame)			{ m_Frame = newFrame; };
	md2_model_t*		getPlayerModel();
	void keyInput( int dir, bool val);

	float getForwardX()						{	return m_vPlayerForward[0];	};

	float	getDirection(int i)					{	return m_vPlayerDir[i];	};
	M3DVector3f		m_vPlayerForward;

	///////////////
	//i think the origin is top left
	//could translate the model
	GLFrame getPosFrame()		{	return m_Pos;	};

	float getVelX()				{	return vVel[0];	};
	float getVelZ()				{	return vVel[2];	};

	float getPosX()				{	return (m_Pos.GetOriginX());	};
	float getPosY()				{	return (m_Pos.GetOriginY());	};
	float getPosZ()				{	return (m_Pos.GetOriginZ());	};

	float getRenderY()			{	return posY;	};

	float getRadius()			{	return radius;	};
	bool getBounds()			{	return bounds;	};
	bool getOnGround()			{	return m_bOnGround;	};

private:

	GLFrame				m_Frame;
	GLFrame				m_Pos;

	ALuint*	 walkSounds[NO_WALK_SONDS];
	CRandom r;

	M3DVector3f vVel;

	int		lvlCenX;
	int		lvlCenZ;

	float linear;
	float rotation;
	float theta, alpha;
	float posX,posY,posZ;
	float updateY;

	void updateLightPos();
	void updateHeight();
	void applyFriction();
	void playWlkSnd();
	bool checkBound(int coord);
	bool checkBounds();
	void resetPos();
	bool checkGradient();

	Spawn*				m_cSpawn;
	M3DVector3f			m_vPlayerDir;
	M3DVector3f			m_vPlayerSide;
	M3DVector4f			m_vLightEyePos;
	M3DVector4f			m_vLightPos;

	md2model*			m_cModelLoader;
	GLuint				texture;
	ALuint				soundID;


	float				radius;
	float				scale;
	
	SoundManager*		m_pSndManager;
	PlayerCamera*		pCamera;
	FirstTerrain*		p_crtLvl;
	int					lvlBound;
	int					returnCount;
	int					crntAnim;
	int					fallCount;
	bool				bounds;
	bool				m_bOnGround;
};

#endif