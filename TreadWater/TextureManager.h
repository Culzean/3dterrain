//
////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604
#define WIN32
#include <map>
#include <string>

#include <GLBatch.h>
#include <GLTools.h>
#include <GLShaderManager.h>
#include <iostream>
#include <assert.h>

using namespace std;

#define FREEGLUT_STATIC
#include <GL/glut.h>

class TextureManager {
private:
	static TextureManager*		pTexManager;
	std::map<std::string, GLuint> Texmap;
	std::map<std::string, GLuint>::iterator index;
	static int lastID;
public:
	//method to get singleton instance
	static TextureManager* GetInstance(){
	if(!pTexManager)
		pTexManager = new TextureManager();
	return pTexManager;
	
	}
	static void DestroyInstance(){
		if(pTexManager)
			delete pTexManager;	pTexManager = NULL;

	}
	~TextureManager();
	TextureManager();
	GLuint loadTexture(const char* fname);
	GLuint getTexID(const char* fname);
	void cleanUp();
};