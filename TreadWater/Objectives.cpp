////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#include "Objectives.h"

Objectives::Objectives()
{

}

Objectives::~Objectives()
{
	delete[] m_aObs;
	m_pCrntMap = NULL;
}

void Objectives::Init( FirstTerrain* pMap )
{
	m_pSndMan = SoundManager::GetInstance();

	snd1 = m_pSndMan->loadSound("res/crowd1.wav");
	snd2 = m_pSndMan->loadSound("res/crowd2.wav");

	m_bComplete = false;
	m_iCollect = 0;
	m_iTestRad = OBJECT_RADIUS * 3.9;

	m_pCrntMap = pMap;
	m_iCount = m_pCrntMap->getSize() / 42 ;
	
	if(m_iCount > MAX_OBJECTIVES)
		m_iCount = MAX_OBJECTIVES;

	m_fMaxDist = m_pCrntMap->getSize() / m_iCount;
	m_fMinDist = m_fMaxDist * 0.2;

	placeObjectives();
}

void Objectives::Update()
{
	for( int i=0; i< m_iCount ; ++i )
	{
		if(m_aObs[i]->update()){
			playGoalSnd();
			++m_iCollect;
		}
	}

	if( m_iCollect >= m_iCount )
		m_bComplete = true;
}

void Objectives::playGoalSnd()
{
	CRandom r;
	r.Randomize();

	float rand = r.Random(40);
	if(rand > 20)
		m_pSndMan->playSound(snd1);
	else
		m_pSndMan->playSound(snd2);
}

bool Objectives::checkPlayer( PlayerUI* pPlayer )
{
	for(int i=0; i < m_iCount; ++i)
	{
		if(m_aObs[i]->checkPlayer( pPlayer, m_iTestRad ))
		{
			// if within the area of the goal increment the counter
			//check collision
			m_aObs[i]->incrGoal();
			pPlayer->checkCollision(m_aObs[i]);
		}
	}
	return true;
}

void Objectives::Render( GLShaderManager &shaderManager, GLGeometryTransform &transformPipeline, GLMatrixStack &modelViewMatrix, M3DMatrix44f mCamera )
{
	for(int i=0; i< m_iCount ; i++)
	{
			m_aObs[i]->render(shaderManager, transformPipeline, modelViewMatrix, mCamera);
	}

}

void Objectives::placeObjectives()
{
	CRandom r;
	r.Randomize();
	int prevX = 0;
	int mapBounds = m_pCrntMap->getSize();
	for(int i=0; i< m_iCount ; i++)
	{
		//int stX = (prevX + r.Random( m_fMaxDist, m_fMinDist ));
		int stX = prevX + m_fMaxDist;
		//int stZ = (prevZ + (r.Random( m_fMaxDist, m_fMinDist )) * 2 - m_fMaxDist);
		int stZ = r.Random(m_pCrntMap->getSize());
		int stY = m_pCrntMap->GetScaledHeightAtPoint( stX, stZ );

		if(stX > (mapBounds) || stZ > mapBounds)
			cout << "Error placing objecives. Beyond Map bounds!!";

		m_pCrntMap->flattenSquare( stX, stZ, OBJECT_RADIUS * 3.3, stY );

		m_aObs[i] = new Object( "res/WaterDisp.md2", "res/waterdisp.tga" );
		m_aObs[i]->init( stX, stY, stZ, OBJECT_RADIUS );

		prevX = stX;
	}
}