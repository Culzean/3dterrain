

#ifndef TOOLS_H
	#define TOOLS_H

#include "stafx.h"

class Tools
{
public:

	Tools();
	~Tools();

	/////////////////////////////////////////////////////////////
	//////////////////////////////////////
	//
	//This is a reworked annd updated gllookat method.
	//thanks to anonymous from www.opengl.org
	static void glLookAt2f(M3DMatrix44f matrix, M3DVector3f eyePos3D, M3DVector3f viewPos3D, M3DVector3f upVector3D){
		M3DVector3f forward, side, up;
		M3DMatrix44f matrix2, resultMatrix;

		m3dSubtractVectors3(forward, viewPos3D, eyePos3D);
		m3dNormalizeVector3(forward);
		//Side = forward X up
		m3dCrossProduct3(side, forward, upVector3D);
		m3dNormalizeVector3(side);
		//recompute up as: up = side X forward
		m3dCrossProduct3(up, side, forward);

		//copy side, up and -forward into 1st,2nd and 3rd rows
		matrix2[0] = side[0];
		matrix2[4] = side[1];
		matrix2[8] = side[2];
		matrix2[12] = 0.0f;

		matrix2[1] = up[0];
		matrix2[5] = up[1];
		matrix2[9] = up[2];
		matrix2[12] = 0.0f;

		matrix2[2] = -forward[0];
		matrix2[6] = -forward[1];
		matrix2[10] = -forward[2];
		matrix2[12] = 0.0f;

		//identity on last row?
		matrix2[3] = matrix2[7] = matrix2[11] = 0.0f;
		matrix2[15] = 1.0f;

		m3dMatrixMultiply44( resultMatrix, matrix, matrix2 );

		glTranslate2f(resultMatrix, eyePos3D[0], eyePos3D[1], eyePos3D[2] );

		memcpy( matrix, resultMatrix, 16 * sizeof(float) );

	};

	//////////////////////////////////////////////
	//manipulate matrix into correct location
	//in this case push camera into desired position
	//code snippet from glhlib, reworked for compliance with math3d
	static void glTranslate2f(M3DMatrix44f &matrix, float camPosX, float camPosY, float camPosZ )
	{
		matrix[12] = matrix[0]*camPosX + matrix[4]*camPosY + matrix[8]*camPosZ + matrix[12];
		matrix[13] = matrix[1]*camPosX + matrix[5]*camPosY + matrix[9]*camPosZ + matrix[13];
		matrix[14] = matrix[2]*camPosX + matrix[6]*camPosY + matrix[10]*camPosZ + matrix[14];
		matrix[15] = matrix[3]*camPosX + matrix[7]*camPosY + matrix[11]*camPosZ + matrix[15];
	};

	

private:



};

	#endif