////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#include "game.h"

GameClass*		newGame;


bool Init(void) 
{
	int size = -1; int hilly = -1;
	cout << "You shall be given control of an alien metamorphizising exploration probe." << endl
	<< "This exploraion device shall be rendered as a object reconisable in your human culture" << endl
	<< "Use this device to explore this alien, urban envirnoment" << endl
	<< "Find all of the marker beacons. These have been rendered as very reconizable to your human eyes" << endl
	<< endl << endl << endl;
	cout << "Here are some choses on the size and form of envirnoment to explore." << endl;

	do{
		cout << "choose the size of map" << endl;
		cout << "0. \t" << "small" << endl;
		cout << "1. \t" << "medium" << endl;
		cout << "2. \t" << "large" << endl;
		cin >> size;
		if(size > 2)
			size = 1;
	}while(size < 0);

	do{
		cout << "would you like to explore a hilly or flat envirnoment?" << endl;
		cout << "0. \t" << "flat" << endl;
		cout << "1. \t" << "hilly" << endl;
		cout << "2. \t" << "very hilly" << endl;
		cin >> size;
		if(size > 2)
			size = 1;
	}while(size < 0);

	if(newGame->Init(size, hilly))
		return true;
	else
		return false;
}

void KeyDown(unsigned char key, int x, int y)
{
	newGame->KeyDown(key,x,y);
}
void KeyUp (unsigned char key, int x, int y) 
{   
	newGame->KeyUp(key,x,y);    
}  

void MouseFunc(int button, int state, int x, int y)
{
	newGame->mouseFunc(button, state, x, y);
}

void MotionFunc(int x, int y)
{
	newGame->mouseMotion(x,y);
}

void Update(void)
{
	newGame->Update();
}

void Display(void) 
{
	newGame->Display();
}

///////////////////////////////////////////////////
// Screen changes size or is initialized
void ChangeSize(int nWidth, int nHeight)
    {
		newGame->Reshape(nWidth, nHeight);
    }

int main(int argc, char **argv) 
{
	newGame = GameClass::GetInstance();
	newGame->SetupWindow(argc, argv);
		
	if(Init())
		cout << "new game initialized and ready to start" << endl;
	else{
		cout << "error in game initialization. aborted load" << endl;
		GameClass::DestroyInstance();
		return 0;
	}
  glutKeyboardFunc(KeyDown);
  glutKeyboardUpFunc(KeyUp);
  glutMouseFunc(MouseFunc);
  glutMotionFunc(MotionFunc);
  glutReshapeFunc(ChangeSize);
  glutDisplayFunc(Display);
  glutPostRedisplay();
  glutIdleFunc(Display);
  glutMainLoop();
  GameClass::DestroyInstance();
  newGame = NULL;
  return 0;
}