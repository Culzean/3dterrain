
////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604
#include "SoundManager.h"

SoundManager* SoundManager::pSoundManager = 0;

SoundManager::SoundManager()
{

}
SoundManager::~SoundManager(){}

int SoundManager::endWithError (char* msg, const char* fname , int error = 0)
{
	cout << msg << " with file " << fname << endl;
	system("PAUSE");
	return error;
}

void SoundManager::playSound( ALuint sndID )
{
	alSourcePlay(sndID);
}

int SoundManager::endWithError (char* msg , int error = 0)
{
	cout << msg << error << endl;
	system("PAUSE");
	return error;
}

bool SoundManager::setUp()
{
	device = alcOpenDevice(NULL);
	context = alcCreateContext(device, NULL);
	alcMakeContextCurrent(context);

	if(!context)
		return endWithError("Context has failed to open");
	if(!device)
		return endWithError("no sound device");

	//assign some information for opengl to track sound and position
	SourcePos[0] = 0.0f;		SourcePos[1] = 0.0f;		SourcePos[2] = 0.0f;
	SourceVel[0] = 0.0f;		SourceVel[1] = 0.0f;		SourceVel[2] = 0.0f;
	ListenerPos[0] = 0.0f;		ListenerPos[1] = 0.0f;		ListenerPos[2] = 0.0f;
	ListenerVel[0] = 0.0f;		ListenerVel[1] = 0.0f;		ListenerVel[2] = 0.0f;
	ListenerOri[0] = 0.0f;		ListenerOri[1] = 0.0f;		ListenerOri[2] = -1.0f;
	ListenerOri[3] = 0.0f;		ListenerOri[4] = 1.0f;		ListenerOri[5] = 0.0f;

	//Listener                                                                               
    alListenerfv(AL_POSITION,    ListenerPos);                                  //Set position of the listener
    alListenerfv(AL_VELOCITY,    ListenerVel);                                  //Set velocity of the listener
    alListenerfv(AL_ORIENTATION, ListenerOri); 

	return true;
}

ALuint SoundManager::loadSound( const char* fname )
{
	FILE *fp = NULL;
	fp = fopen(fname, "rb");

	if(!fp)
		endWithError("There was a problem opening this file: ", fname);

	//set variables to store the sound
	char type[4];
	DWORD size, chunkSize;
	short formatType, channels;
	DWORD sampleRate, avgBytesPerSec;
	short bytesPerSample, bitsPerSample;
	DWORD dataSize;

	//read the file header
	//a sensible place to start with any f read
	//read the first bytes in the file, what should they be?
	fread (type, sizeof(char), 4, fp);
	if(type[0] != 'R' || type[1] != 'I' || type[2] != 'F' || type[3] != 'F')		//RIFF check
		return endWithError("No RIFF");

	fread(&size, sizeof(DWORD), 1, fp );
	fread(type, sizeof(char) , 4, fp );
	if(type[0] != 'W' || type[1] != 'A' || type[2] != 'V' || type[3] != 'E')		//WAVE check
		return endWithError("Not WAVE");

	fread(type, sizeof(char),  4, fp );
	if(type[0] != 'f' || type[1] != 'm' || type[2] != 't' || type[3] != ' ')		//fmt check?
		return endWithError("not fmt ");

	//so this is a wave file
	//how do we digest this data? don't all of these to play a sound
	fread( &chunkSize, sizeof(DWORD), 1, fp );
	fread( &formatType, sizeof(short), 1, fp );
	fread( &channels, sizeof(short), 1, fp );
	fread( &sampleRate, sizeof(DWORD), 1, fp );
	fread( &avgBytesPerSec, sizeof(DWORD), 1, fp );
	fread( &bytesPerSample, sizeof(short), 1, fp );
	fread( &bitsPerSample, sizeof(short), 1, fp );

	fread( type, sizeof(char), 4, fp );
	if(type[0] != 'd' || type[1] != 'a' || type[2] != 't' || type[3] != 'a')
		//check we are at the data now
		return endWithError("No data");

	//how large is this file?
	fread( &dataSize, sizeof(DWORD), 1, fp );

	//Display the info about the WAVE file
    cout << "Chunk Size: " << chunkSize << "\n";
    cout << "Format Type: " << formatType << "\n";
    cout << "Channels: " << channels << "\n";
    cout << "Sample Rate: " << sampleRate << "\n";
    cout << "Average Bytes Per Second: " << avgBytesPerSec << "\n";
    cout << "Bytes Per Sample: " << bytesPerSample << "\n";
    cout << "Bits Per Sample: " << bitsPerSample << "\n";
    cout << "Data Size: " << dataSize << "\n";

	//open up a new buffer
	unsigned char* buf = new unsigned char[dataSize];
	cout << fread(buf, sizeof(BYTE), dataSize, fp) << " bytes loaded" << endl;

	//some important variables for loading a sound file
	//and binding it
	ALuint* source = new ALuint;			//a name
				//where we are storing the sound
	ALuint frequency = sampleRate;
	ALenum format = 0;		//important information about this wav

	alGenBuffers(1, &buffer);	//generate one buffer and link to my variable
	alGenSources(1, source);	//generate one source and link to my variable
	//if(alGetError != AL_NO_ERROR)
	//	return endWithError("Oh NO AL. Generating sources", (int)alGetError);

	//a bit of logic to figure the format of the wave
	//only 8 or 16 bit?
	if(bitsPerSample == 8)
	{
		if(channels == 1)
			format = AL_FORMAT_MONO8;
		else if(channels == 2)
			format == AL_FORMAT_STEREO8;
	}
	else if(bitsPerSample == 16)
	{
		if(channels == 1)
			format = AL_FORMAT_MONO16;
		else if(channels == 2)
			format == AL_FORMAT_STEREO16;
	}//discard other formats!
	if(!format)
		return endWithError("Can not read this bits per sample");

	alBufferData( buffer, format, buf, dataSize, frequency );
	//if(alGetError() != AL_NO_ERROR)
	//	endWithError("error loading ALbuffer");

	//can bind this buffer to source
	//can make a few settings
	alSourcei(*source, AL_BUFFER, buffer);
	alSourcef(*source, AL_PITCH, 1.0f);
	alSourcef(*source, AL_GAIN, 1.0f);
	alSourcefv(*source, AL_POSITION, SourcePos);
	alSourcefv(*source, AL_VELOCITY, SourceVel);
	alSourcei(*source, AL_LOOPING, AL_FALSE);

	SoundMap.insert( pair<string, ALuint>( fname, *source) );

	fclose(fp);
	delete source;
	delete[] buf;
	cout << "New sound loaded: " << fname << endl;
	return getSoundID(fname);
}


ALuint SoundManager::getSoundID(const char* fname)
{
	index = SoundMap.find(fname);

	if(index == SoundMap.end())
		{
			if(loadSound(fname) == -1)//load failed
			{
				cout << "This texture cannot be found" << endl;
			}
		}

	return SoundMap[fname];
}

void SoundManager::cleanUp()
{
	index = SoundMap.begin();
//	for(; index != SoundMap.end(); index++)
	{
		alDeleteSources(SoundMap.size(), &(*index).second );
		alDeleteBuffers(1, &(buffer));
	}

	alcMakeContextCurrent(NULL);
	alcDestroyContext(context);                                                 //Destroy the OpenAL Context
    alcCloseDevice(device);
	delete device;
	delete context;
}