////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#include <al.h>
#include <alc.h>
#include <cstdlib>
#include <Windows.h>
#include <iostream>
#include <map>
#include <string>

using namespace std;

class SoundManager {
private:
	static SoundManager*		pSoundManager;
	std::map<std::string, ALuint> SoundMap;
	std::map<std::string, ALuint>::iterator index;
	static int lastID;
	bool setUp();

	int endWithError (char* msg, const char* fname , int error);
	int endWithError (char* msg , int error);

	ALCdevice* device;
	ALCcontext* context;
	ALuint buffer;

	//some data members
	ALfloat SourcePos[3];                                    //Position of the source sound
    ALfloat SourceVel[3];                                    //Velocity of the source sound
    ALfloat ListenerPos[3];                                  //Position of the listener
    ALfloat ListenerVel[3];                                  //Velocity of the listener
    ALfloat ListenerOri[6];					//Orientation of the listener

public:
	//method to get singleton instance
	static SoundManager* GetInstance(){
	if(!pSoundManager)
		{
			pSoundManager = new SoundManager();
			pSoundManager->setUp();
		}
	return pSoundManager;
	
	}
	static void DestroyInstance(){
		if(pSoundManager)
			delete pSoundManager;	pSoundManager = NULL;

	}
	~SoundManager();
	SoundManager();
	void playSound( ALuint sndID );
	ALuint loadSound(const char* fname);
	ALuint getSoundID(const char* fname);
	void cleanUp();
};