////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#ifndef	SKY_BOX_H
#define	SKY_BOX_H

#define WIN32

#include <GLTools.h>
#include <GLShaderManager.h>
#include <GLBatch.h>

#include <GLFrame.h>
#include <GLMatrixStack.h>
#include <GLGeometryTransform.h>

#ifdef __APPLE__
#include <glut/glut.h>
#else
#define FREEGLUT_STATIC
#include <GL/glut.h>
#endif

using namespace std;

#include <GL/freeglut.h>

////////////////////////////////////////
//
//Class to load 6 textures and a custom shader
//then display them using rotOnly on a cube and no depth buffer
//must be rendered first!

class SkyBox{
private:
	GLenum m_enSky[6];
	GLint			m_iSkyBoxShader;
	GLint			m_iLocMVPSky;

	GLuint			cubeTexture;

	GLBatch			skyBatch;
	GLFrame			viewFrame;

	void LoadSky();

public:
	void Render(GLFrame cameraFrame, GLMatrixStack &modelViewMatrix, GLGeometryTransform &transformPipeline);
	void Init();
	void CleanUp();

	SkyBox();
	~SkyBox();
};

#endif