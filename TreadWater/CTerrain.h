////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#ifndef	TERRAIN_H

#define TERRAIN_H

#define WIN32

#define FREEGLUT_STATIC


#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include <GLTools.h>

#include "CRandom.h"

using namespace std;

struct SHEIGHT_DATA
{
	unsigned char* m_ucpData;			//this is where we will write the height data into
	int m_iSize;						//this is the size of data and must be a power of 2
};

class CTerrain{
	//This is the base class for creating lots of terrain
protected:
	SHEIGHT_DATA	m_heightData;

	GLfloat			m_fHeightScale;

	int				m_iVertsPerFrame;		//some console info
	int				m_iTrisPerFrame;

	void FilterHeightBand( float* fpBand, int iStride, int iCount, float fFilter );
	void FilterHeightField( float* fpHeightData, float iFilter);

	int				m_iSize;
	bool			m_bBuilt;

public:

	int	getSize()			{	return m_iSize;	};


	//virtual void Render(void) = 0;

	bool LoadHeightMap( char* szFileName, int iSize );
	bool SaveHeightMap( char* szFileName );
	bool UnloadHeightMap( void );

	void NormalizeHeightBuffer( float* fpHeightData );

	inline int GetNumberOfVertsPerFrame( void ) { return m_iVertsPerFrame; };
	inline int GetNumberOfTrisPerFrame( void ) { return m_iTrisPerFrame; };

	///////////////////////////////////////////////////////////////////
	//
	//Method to create our own new height map
	//Using a fault line algorithm. Fill the buffer with full values. draw a line across the hieght map
	//and everything below this line is reduced in height value. REPEAT
	//Will pass across this with a smoothing algorithm
	bool MakeTerrainFault( int iSize, int iIterations, int iMinDelta, int iMaxDelta, float fFilter);

	////////////////////////////////////////////
	//set scale here
	//set height here
	inline void SetHeightScale( GLfloat fScale )	{ m_fHeightScale = fScale; };
	inline void SetHeightAtPoint( unsigned char ucHeight, int iX, int iZ )
	{ m_heightData.m_ucpData[( iZ*m_iSize) + iX ] = ucHeight; };			//will write this straight in from fRead

	inline GLfloat GetHeightScale()					{	return m_fHeightScale;	};

	///////////////////////////////////////////////////////////////////////
	///
	//getters for access struct for height information both scaled and unscaled
	///
	//returned as pure ucpData or a float
	inline unsigned char GetTrueHeightAtPoint( int iX, int iZ )
	{ return ( m_heightData.m_ucpData[ (iZ*m_iSize ) + iX ]); };

	inline GLfloat GetScaledHeightAtPoint( int iX, int iZ )
		{ return ( m_heightData.m_ucpData[ (iZ*m_iSize ) + iX ] * m_fHeightScale); };

	CTerrain(void)	{	}
	~CTerrain()		{	}

};

#endif