////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#pragma once

#define WIN32


#include <stdio.h>
#include <iostream>
#include <ctime>

#include <GLTools.h>
#include <GLShaderManager.h>
#include <GLFrustum.h>
#include <GLBatch.h>
#include <GLMatrixStack.h>
#include <GLGeometryTransform.h>
#include <StopWatch.h>

#include <math3d.h>
#include <math.h>

#ifdef __APPLE__
#include <glut/glut.h>
#else
#define FREEGLUT_STATIC
#include <GL/glut.h>
#endif

#include <sstream>
#include <GL/freeglut.h>
#include "FirstTerrain.h"
#include "SkyBox.h"
#include "md2model.h"
#include "Spawn.h"
#include "TextureManager.h"
#include "SoundManager.h"
#include "Objectives.h"
#include "PlayerUI.h"
#include "Object.h"
#include "bass.h"

#define _USE_MATH_DEFINES

#define RETURN_COUNTER			120

static M3DVector3f forwardV = {0.0f,0.0f,-1.0f};
static M3DVector3f upV = { 0.0f, 1.0f, 0.0f };
//these are very difficult values to pass between classes

using namespace std;