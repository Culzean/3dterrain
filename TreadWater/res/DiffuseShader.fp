// Terrain Shader
// Fragment Shader
// by Daniel Waine
// interpolated from examples by
// Richard S. Wright Jr.
// OpenGL SuperBible
#version 130

in vec2 vVaryingTexCoords;

out vec4 vFragColor;
in vec4 vVaryingColor;

void main(void)
   { 
   vFragColor = vVaryingColor;
   }